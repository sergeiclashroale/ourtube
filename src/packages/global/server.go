package global

import (
	"context"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"go.uber.org/fx"
	"net/http"
	"strconv"
)

func NewServer(lc fx.Lifecycle, router *gin.Engine, log *logrus.Logger, config *Config) *http.Server {
	httpServer := &http.Server{
		Addr:    config.ServerHost + ":" + strconv.Itoa(config.ServerPort),
		Handler: router,
	}

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			log.WithFields(logrus.Fields{
				"host": config.ServerHost,
				"port": strconv.Itoa(config.ServerPort),
			}).Info("Server started")

			go func() {
				err := httpServer.ListenAndServe()
				if err != nil {
					log.WithFields(logrus.Fields{
						"error": err.Error(),
					}).Error("Server aborted to start")
				}
			}()

			return nil
		},

		OnStop: func(ctx context.Context) error {
			log.Info("Server stopped")
			return httpServer.Shutdown(ctx)
		},
	})

	return httpServer
}
