package entity

import (
	"accountant/packages/general"
)

type AccountChange struct {
	general.Model
	/*
		If positive - income
		If negative - expense
	*/
	Value                   float64 `json:"value"`
	Name                    string  `json:"name"`
	AccountID               uint    `json:"account_id"`
	AccountChangeCategoryID uint    `json:"category_id" gorm:"default:null"`
}
