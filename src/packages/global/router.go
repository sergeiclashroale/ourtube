package global

import (
	"accountant/packages/general"
	"github.com/gin-gonic/gin"
	"go.uber.org/fx"
)

func GetRouterModule() fx.Option {
	return fx.Provide(
		fx.Annotate(
			NewRouter,
			fx.ParamTags(`group:"handlers"`),
		),
	)
}

func NewRouter(controllers []general.Handler, config *Config) *gin.Engine {
	router := gin.Default()

	// CORS
	router.Use(func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", config.CorsDomain)
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,PATCH,DELETE")
		c.Header("Access-Control-Allow-Headers", "*")
		c.Next()
	})

	// Register handlers
	api := router.Group("/api")
	for _, controller := range controllers {
		controller.Handle(api)
	}

	return router
}
