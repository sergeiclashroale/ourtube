package dto

type AccountCreateDto struct {
	Name    string                   `json:"name" binding:"required"`
	Changes []AccountChangeCreateDto `json:"changes" binding:"required,dive,required"`
}
