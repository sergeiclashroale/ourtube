package docs

import (
	"github.com/gin-gonic/gin"
)

type RoutesHandler struct {
}

func NewDocsRoutesHandler() *RoutesHandler {
	return &RoutesHandler{}
}

func (h RoutesHandler) Handle(group *gin.RouterGroup) {
	group.Static("/docs", "./packages/docs/dist")
}
