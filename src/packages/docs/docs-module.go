package docs

import (
	"accountant/packages/general"
	"go.uber.org/fx"
)

func GetDocsModule() fx.Option {
	return fx.Provide(
		// Handler
		general.AsHandler(NewDocsRoutesHandler),
	)
}
