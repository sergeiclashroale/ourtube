package global

import (
	"github.com/sirupsen/logrus"
	"go.uber.org/fx"
	"os"
)

func GetLoggerModule() fx.Option {
	return fx.Provide(NewLogger)
}

func NewLogger() *logrus.Logger {
	var log = logrus.New()
	log.Out = os.Stdout
	return log
}
