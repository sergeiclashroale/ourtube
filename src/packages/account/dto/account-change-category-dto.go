package dto

type AccountChangeCategoryCreateDto struct {
	Name string `json:"name" binding:"required"`
}
