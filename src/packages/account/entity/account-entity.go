package entity

import (
	"accountant/packages/general"
)

type Account struct {
	general.Model
	Changes []AccountChange `json:"changes"`
	Name    string          `json:"name"`
}
