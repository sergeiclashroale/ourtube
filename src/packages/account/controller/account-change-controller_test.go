package controller

import (
	"accountant/packages/account/dto"
	"accountant/packages/account/entity"
	"bytes"
	"encoding/json"
	"errors"
	"github.com/gin-gonic/gin"
	"net/http"
	"net/http/httptest"
	"testing"
)

type accountChangeRepositoryMock struct {
}

func (r accountChangeRepositoryMock) GetAccountChanges(accountId uint) ([]entity.AccountChange, error) {
	return []entity.AccountChange{
		{
			AccountID: accountId,
		},
	}, nil
}

func (r accountChangeRepositoryMock) CreateAccountChange(dto dto.AccountChangeCreateDto, accountId uint) (entity.AccountChange, error) {
	return entity.AccountChange{
			AccountID: accountId,
			Name:      dto.Name,
		},
		nil
}

type accountChangeRepositoryMockFailed struct {
}

func (r accountChangeRepositoryMockFailed) GetAccountChanges(accountId uint) ([]entity.AccountChange, error) {
	return []entity.AccountChange{
		{
			AccountID: accountId,
		},
	}, errors.New("failed to get account changes")
}

func (r accountChangeRepositoryMockFailed) CreateAccountChange(dto dto.AccountChangeCreateDto, accountId uint) (entity.AccountChange, error) {
	return entity.AccountChange{
		Name:      dto.Name,
		AccountID: accountId,
	}, errors.New("failed to create account change")
}

func TestGetAccountChanges_should_return_account_changes(t *testing.T) {
	// Given
	controller := NewAccountChangeController(accountChangeRepositoryMock{})

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)
	ctx.Request = httptest.NewRequest("GET", "/accounts/changes/1", nil)
	ctx.Params = gin.Params{
		{
			Key:   "id",
			Value: "1",
		},
	}

	// When
	controller.GetAccountChanges(ctx)

	// Then
	if w.Code != 200 {
		t.Error("Expected status code 200, got ", w.Code)
	}

	var accountChanges []entity.AccountChange
	decoder := json.NewDecoder(w.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&accountChanges)
	if err != nil {
		t.Errorf("Expected response body to be account changes type, got %s", err.Error())
	}
}

func TestGetAccountChanges_should_return_error_when_id_is_not_number(t *testing.T) {
	// Given
	controller := NewAccountChangeController(accountChangeRepositoryMock{})

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)
	ctx.Params = gin.Params{
		{
			Key:   "id",
			Value: "b",
		},
	}

	// When
	controller.GetAccountChanges(ctx)

	// Then
	if w.Code != http.StatusBadRequest {
		t.Errorf("Expected status code 400, got %d", w.Code)
	}
}

func TestGetAccountChanges_should_return_error_when_repository_failed(t *testing.T) {
	// Given
	controller := NewAccountChangeController(accountChangeRepositoryMockFailed{})

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)
	ctx.Params = gin.Params{
		{
			Key:   "id",
			Value: "1",
		},
	}

	// When
	controller.GetAccountChanges(ctx)

	// Then
	if w.Code != http.StatusInternalServerError {
		t.Errorf("Expected status code 500, got %d", w.Code)
	}
}

func TestCreateAccountChange_should_return_created_account_change(t *testing.T) {
	// Given
	controller := NewAccountChangeController(accountChangeRepositoryMock{})

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)

	ctx.Params = gin.Params{
		{
			Key:   "id",
			Value: "1",
		},
	}
	accountChangeCreateDto := dto.AccountChangeCreateDto{
		Value:                   100,
		Name:                    "Test",
		AccountChangeCategoryID: 1,
	}
	body, _ := json.Marshal(accountChangeCreateDto)
	ctx.Request = httptest.NewRequest("POST", "/accounts/changes/1", bytes.NewReader(body))

	// When
	controller.CreateAccountChange(ctx)

	// Then
	if w.Code != 201 {
		t.Error("Expected status code 201, got ", w.Code)
	}

	var accountChange entity.AccountChange
	decoder := json.NewDecoder(w.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&accountChange)
	if err != nil {
		t.Errorf("Expected response body to be account change type, got %s", err.Error())
	}
}

func TestCreateAccountChange_should_return_error_when_id_is_not_number(t *testing.T) {
	// Given
	controller := NewAccountChangeController(accountChangeRepositoryMock{})

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)

	ctx.Params = gin.Params{
		{
			Key:   "id",
			Value: "b",
		},
	}
	accountChangeCreateDto := dto.AccountChangeCreateDto{
		Value:                   100,
		Name:                    "Test",
		AccountChangeCategoryID: 1,
	}
	body, _ := json.Marshal(accountChangeCreateDto)
	ctx.Request = httptest.NewRequest("POST", "/accounts/changes/1", bytes.NewReader(body))

	// When
	controller.CreateAccountChange(ctx)

	// Then
	if w.Code != http.StatusBadRequest {
		t.Errorf("Expected status code 400, got %d", w.Code)
	}
}

func TestCreateAccountChange_should_return_error_when_body_is_not_valid(t *testing.T) {
	// Given
	controller := NewAccountChangeController(accountChangeRepositoryMock{})

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)

	ctx.Params = gin.Params{
		{
			Key:   "id",
			Value: "1",
		},
	}
	body, _ := json.Marshal("test")
	ctx.Request = httptest.NewRequest("POST", "/accounts/changes/1", bytes.NewReader(body))

	// When
	controller.CreateAccountChange(ctx)

	// Then
	if w.Code != http.StatusBadRequest {
		t.Errorf("Expected status code 400, got %d", w.Code)
	}
}

func TestCreateAccountChange_should_return_error_repository_failed(t *testing.T) {
	// Given
	controller := NewAccountChangeController(accountChangeRepositoryMockFailed{})

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)

	ctx.Params = gin.Params{
		{
			Key:   "id",
			Value: "1",
		},
	}
	accountChangeCreateDto := dto.AccountChangeCreateDto{
		Value:                   100,
		Name:                    "Test",
		AccountChangeCategoryID: 1,
	}
	body, _ := json.Marshal(accountChangeCreateDto)
	ctx.Request = httptest.NewRequest("POST", "/accounts/changes/1", bytes.NewReader(body))

	// When
	controller.CreateAccountChange(ctx)

	// Then
	if w.Code != http.StatusInternalServerError {
		t.Errorf("Expected status code 500, got %d", w.Code)
	}
}
