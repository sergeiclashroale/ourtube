FROM golang:1.20-alpine as builder
LABEL authors="sergei888"

# Set destination for COPY
WORKDIR /accountant/src

# Download Go modules
COPY ./src/go.mod ./src/go.sum ./
RUN go mod download

# Copy the source code
COPY ./src .

# Build
RUN go build -o /accountant/dist

EXPOSE 8080

# Run the binary
RUN /accountant/dist
