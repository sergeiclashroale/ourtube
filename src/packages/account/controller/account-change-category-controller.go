package controller

import (
	"accountant/packages/account/repository"
	"github.com/gin-gonic/gin"
)

type AccountChangeCategoryController struct {
	r repository.IAccountChangeCategoryRepository
}

func NewAccountChangeCategoryController(r repository.IAccountChangeCategoryRepository) *AccountChangeCategoryController {
	return &AccountChangeCategoryController{
		r,
	}
}

func (c AccountChangeCategoryController) CreateAccountChangeCategory(ctx *gin.Context) {
	ctx.JSON(201, gin.H{
		"name": "account change category created",
	})
}
