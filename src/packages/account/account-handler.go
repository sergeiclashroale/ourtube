package account

import (
	controller2 "accountant/packages/account/controller"
	"github.com/gin-gonic/gin"
)

type RoutesHandler struct {
	accountController       *controller2.AccountController
	accountChangeController *controller2.AccountChangeController
}

func NewAccountRoutesHandler(accountController *controller2.AccountController, accountChangeController *controller2.AccountChangeController) *RoutesHandler {
	return &RoutesHandler{
		accountController:       accountController,
		accountChangeController: accountChangeController,
	}
}

func (h RoutesHandler) Handle(group *gin.RouterGroup) {
	accounts := group.Group("/accounts")
	accounts.GET("/", h.accountController.GetAllAccounts)
	accounts.GET("/:id", h.accountController.GetAccountById)
	accounts.POST("/", h.accountController.CreateAccount)
	accounts.GET("/:id/changes", h.accountChangeController.GetAccountChanges)
	accounts.POST("/:id/changes", h.accountChangeController.CreateAccountChange)
}
