package repository

import (
	"accountant/packages/account/dto"
	"accountant/packages/account/entity"
	"errors"
	"go.uber.org/fx"
	"gorm.io/gorm"
)

type IAccountChangeRepository interface {
	GetAccountChanges(accountId uint) ([]entity.AccountChange, error)
	CreateAccountChange(dto dto.AccountChangeCreateDto, accountId uint) (entity.AccountChange, error)
}

type AccountChangeRepository struct {
	db *gorm.DB
}

func AsIAccountChangeRepository(repository any) any {
	return fx.Annotate(
		repository,
		fx.As(new(IAccountChangeRepository)),
	)
}

func NewAccountChangeRepository(db *gorm.DB) *AccountChangeRepository {
	return &AccountChangeRepository{db: db}
}

func (r *AccountChangeRepository) GetAccountChanges(accountId uint) ([]entity.AccountChange, error) {
	var accountChanges []entity.AccountChange

	result := r.db.Where("account_id = ?", accountId).Find(&accountChanges)
	if result.Error != nil && errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return accountChanges, nil
	} else if result.Error != nil {
		return accountChanges, result.Error
	}

	return accountChanges, nil
}

func (r *AccountChangeRepository) CreateAccountChange(dto dto.AccountChangeCreateDto, accountId uint) (entity.AccountChange, error) {
	accountChange := entity.AccountChange{Value: dto.Value, Name: dto.Name, AccountID: accountId}

	result := r.db.Create(&accountChange)
	if result.Error != nil {
		return accountChange, result.Error
	}

	return accountChange, nil
}
