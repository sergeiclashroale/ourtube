package global

import (
	"accountant/packages/account/entity"
	"github.com/sirupsen/logrus"
	"go.uber.org/fx"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"strconv"
)

func GetDatabaseModule() fx.Option {
	return fx.Provide(NewDatabase)
}

func NewDatabase(logger *logrus.Logger, config *Config) *gorm.DB {
	dsn := "host=" + config.DbHost + " user=" + config.DbUser + " password=" + config.DbPassword + " dbname=" + config.DbName + " port=" + strconv.Itoa(config.DbPort) + " sslmode=disable"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		SkipDefaultTransaction: true,
	})

	if err != nil {
		logger.WithFields(logrus.Fields{
			"error": err.Error(),
		}).Error("Failed to connect database")

		panic("Failed to connect database")
	}

	logger.Info("Database connected")

	MigrateDatabase(db)

	return db
}

func MigrateDatabase(db *gorm.DB) {
	err := db.AutoMigrate(&entity.Account{}, &entity.AccountChangeCategory{}, &entity.AccountChange{})
	if err != nil {
		panic("failed to migrate database")
	}
}
