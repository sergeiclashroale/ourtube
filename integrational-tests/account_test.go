//go:build integrational
// +build integrational

package integrational_tests

import (
	"testing"
)

func Benchmark_AccountGetAll(b *testing.B) {
	router := fx_deps.GetRouter()
}

//func Test_AccountGetAll_should_return_no_accounts(t *testing.T) {
//	fx.New(
//		fx_options.GetApplicationOption(),
//		fx.Invoke(func(log *logrus.Logger, router *gin.Engine, db *gorm.DB) {
//			global.ClearDatabase(db)
//
//			w := httptest.NewRecorder()
//			req, _ := http.NewRequest("GET", "/api/accounts/", nil)
//			router.ServeHTTP(w, req)
//
//			assert.Equal(t, 200, w.Code)
//			assert.Equal(t, "[]", w.Body.String())
//		}),
//	)
//}
//
//func Test_AccountGetAll_should_return_account_with_no_changes(t *testing.T) {
//	fx.New(
//		fx_options.GetApplicationOption(),
//		fx.Invoke(func(log *logrus.Logger, router *gin.Engine, db *gorm.DB) {
//			global.ClearDatabase(db)
//
//			account := entity.Account{
//				Name: "Test Account",
//			}
//			db.Create(&account)
//
//			w := httptest.NewRecorder()
//			req, _ := http.NewRequest("GET", "/api/accounts/", nil)
//			router.ServeHTTP(w, req)
//
//			var result []entity.Account
//			err := json.Unmarshal(w.Body.Bytes(), &result)
//			if err != nil {
//				panic(err)
//			}
//
//			assert.Equal(t, 200, w.Code)
//			assert.Equal(t, []entity.Account{account}, result)
//		}),
//	)
//}
