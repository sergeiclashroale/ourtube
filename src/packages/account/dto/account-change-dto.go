package dto

type AccountChangeCreateDto struct {
	Value                   float64 `json:"value" binding:"required"`
	Name                    string  `json:"name" binding:"required"`
	AccountChangeCategoryID uint    `json:"category_id"`
}
