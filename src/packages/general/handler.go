package general

import (
	"github.com/gin-gonic/gin"
	"go.uber.org/fx"
)

func AsHandler(handler any) any {
	return fx.Annotate(
		handler,
		fx.As(new(Handler)),
		fx.ResultTags(`group:"handlers"`),
	)
}

type Handler interface {
	Handle(group *gin.RouterGroup)
}
