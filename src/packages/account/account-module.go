package account

import (
	"accountant/packages/account/controller"
	"accountant/packages/account/repository"
	"accountant/packages/general"
	"go.uber.org/fx"
)

func GetAccountModule() fx.Option {
	return fx.Provide(
		// Accounts
		repository.AsIAccountRepository(repository.NewAccountRepository),
		controller.NewAccountController,

		// Account changes
		repository.AsIAccountChangeRepository(repository.NewAccountChangeRepository),
		controller.NewAccountChangeController,

		// Handler
		general.AsHandler(NewAccountRoutesHandler),
	)
}
