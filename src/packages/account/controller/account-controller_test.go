package controller

import (
	"accountant/packages/account/dto"
	"accountant/packages/account/entity"
	"accountant/packages/account/repository"
	"accountant/packages/general"
	"bytes"
	"encoding/json"
	"errors"
	"github.com/gin-gonic/gin"
	"net/http"
	"net/http/httptest"
	"testing"
)

type accountRepositoryMock struct {
	repository.IAccountRepository
}

func (r accountRepositoryMock) GetAccounts() ([]entity.Account, error) {
	return []entity.Account{
		{

			Name:    "test",
			Changes: nil,
		},
	}, nil
}

func (r accountRepositoryMock) GetAccountById(id uint) (entity.Account, error) {
	return entity.Account{
		Model: general.Model{
			ID: id,
		},
		Name:    "test",
		Changes: nil,
	}, nil
}

func (r accountRepositoryMock) CreateAccount(dto dto.AccountCreateDto) (entity.Account, error) {
	return entity.Account{
		Name:    dto.Name,
		Changes: nil,
	}, nil
}

type accountRepositoryMockFailed struct {
	repository.IAccountRepository
}

func (r accountRepositoryMockFailed) GetAccounts() ([]entity.Account, error) {
	return nil, errors.New("failed to get accounts")
}

func (r accountRepositoryMockFailed) GetAccountById(id uint) (entity.Account, error) {
	return entity.Account{
		Model: general.Model{
			ID: id,
		},
	}, errors.New("failed to get account by id")
}

func (r accountRepositoryMockFailed) CreateAccount(dto dto.AccountCreateDto) (entity.Account, error) {
	return entity.Account{
		Name:    dto.Name,
		Changes: nil,
	}, errors.New("failed to create account")
}

func TestGetAllAccounts_should_return_all_accounts(t *testing.T) {
	// Given
	controller := NewAccountController(accountRepositoryMock{})

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)

	// When
	controller.GetAllAccounts(ctx)

	// Then
	if w.Code != http.StatusOK {
		t.Errorf("Expected status code 200, got %d", w.Code)
	}

	var accounts []entity.Account
	decoder := json.NewDecoder(w.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&accounts)
	if err != nil {
		t.Errorf("Expected response body to be accounts type, got %s", err.Error())
	}
}

func TestGetAllAccounts_should_return_error_when_repository_failed(t *testing.T) {
	// Given
	controller := NewAccountController(accountRepositoryMockFailed{})

	// When
	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)
	controller.GetAllAccounts(ctx)

	// Then
	if w.Code != http.StatusInternalServerError {
		t.Errorf("Expected status code 500, got %d", w.Code)
	}
}

func TestGetAccountById_should_return_account_by_id(t *testing.T) {
	// Given
	controller := NewAccountController(accountRepositoryMock{})

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)
	ctx.Params = gin.Params{
		{
			Key:   "id",
			Value: "1",
		},
	}

	// When
	controller.GetAccountById(ctx)

	// Then
	if w.Code != http.StatusOK {
		t.Errorf("Expected status code 200, got %d", w.Code)
	}

	var account entity.Account
	decoder := json.NewDecoder(w.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&account)
	if err != nil {
		t.Errorf("Expected response body to be account type, got %s", err.Error())
	}
}

func TestGetAccountById_should_return_error_when_id_is_not_number(t *testing.T) {
	// Given
	controller := NewAccountController(accountRepositoryMock{})

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)
	ctx.Params = gin.Params{
		{
			Key:   "id",
			Value: "b",
		},
	}

	// When
	controller.GetAccountById(ctx)

	// Then
	if w.Code != http.StatusBadRequest {
		t.Errorf("Expected status code 400, got %d", w.Code)
	}
}

func TestGetAccountById_should_return_error_when_repository_failed(t *testing.T) {
	// Given
	controller := NewAccountController(accountRepositoryMockFailed{})

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)
	ctx.Params = gin.Params{
		{
			Key:   "id",
			Value: "1",
		},
	}

	// When
	controller.GetAccountById(ctx)

	// Then
	if w.Code != http.StatusInternalServerError {
		t.Errorf("Expected status code 500, got %d", w.Code)
	}
}

func TestCreateAccount_should_return_created_account(t *testing.T) {
	// Given
	controller := NewAccountController(accountRepositoryMock{})

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)
	accountCreateDto := dto.AccountCreateDto{
		Name:    "test",
		Changes: []dto.AccountChangeCreateDto{},
	}
	body, _ := json.Marshal(accountCreateDto)
	ctx.Request = httptest.NewRequest("POST", "/accounts", bytes.NewReader(body))

	// When
	controller.CreateAccount(ctx)

	// Then
	if w.Code != http.StatusCreated {
		t.Errorf("Expected status code 500, got %d", w.Code)
	}

	var account entity.Account
	decoder := json.NewDecoder(w.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&account)
	if err != nil {
		t.Errorf("Expected response body to be account type, got %s", err.Error())
	}
}

func TestCreateAccount_should_return_error_when_repository_failed(t *testing.T) {
	// Given
	controller := NewAccountController(accountRepositoryMockFailed{})
	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)

	accountCreateDto := dto.AccountCreateDto{
		Name:    "test",
		Changes: []dto.AccountChangeCreateDto{},
	}
	body, _ := json.Marshal(accountCreateDto)
	ctx.Request = httptest.NewRequest("POST", "/accounts", bytes.NewReader(body))

	// When
	controller.CreateAccount(ctx)

	// Then
	if w.Code != http.StatusInternalServerError {
		t.Errorf("Expected status code 500, got %d", w.Code)
	}
}

func TestCreateAccount_should_return_error_when_body_is_not_valid(t *testing.T) {
	// Given
	controller := NewAccountController(accountRepositoryMock{})

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)
	ctx.Request = httptest.NewRequest("POST", "/accounts", nil)

	// When
	controller.CreateAccount(ctx)

	// Then
	if w.Code != http.StatusBadRequest {
		t.Errorf("Expected status code 400, got %d", w.Code)
	}
}
