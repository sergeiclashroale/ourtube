package global

import (
	"github.com/spf13/viper"
	"go.uber.org/fx"
)

type Config struct {
	// Server
	ServerHost string `mapstructure:"SERVER_HOST"`
	ServerPort int    `mapstructure:"SERVER_PORT"`
	// Db
	DbHost     string `mapstructure:"DATABASE_HOST"`
	DbPort     int    `mapstructure:"DATABASE_PORT"`
	DbUser     string `mapstructure:"DATABASE_USER"`
	DbPassword string `mapstructure:"DATABASE_PASSWORD"`
	DbName     string `mapstructure:"DATABASE_NAME"`
	// Cors
	CorsDomain string `mapstructure:"CORS_DOMAIN"`
}

func NewConfig() *Config {
	v := viper.New()
	v.SetConfigFile(".env")
	v.AddConfigPath(".")
	v.AutomaticEnv()

	err := v.ReadInConfig()
	if err != nil {
		panic(err)
	}

	c := &Config{}

	err = v.Unmarshal(c)
	if err != nil {
		panic(err)
	}

	return c
}

func GetConfigModule() fx.Option {
	return fx.Provide(NewConfig)
}
