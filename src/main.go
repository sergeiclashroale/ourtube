package main

import (
	"accountant/packages"
	"accountant/packages/global"
	"github.com/joho/godotenv"
	"go.uber.org/fx"
)

func main() {
	err := godotenv.Load(".env")
	if err != nil {
		panic("Error loading .env file")
	}

	fx.New(
		// App
		packages.GetApplicationModule(),
		// Starting server
		fx.Invoke(
			global.NewServer,
		),
	).Run()

	return
}
