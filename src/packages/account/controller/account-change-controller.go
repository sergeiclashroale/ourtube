package controller

import (
	"accountant/packages/account/dto"
	"accountant/packages/account/repository"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type AccountChangeController struct {
	r repository.IAccountChangeRepository
}

func NewAccountChangeController(accountChangeRepository repository.IAccountChangeRepository) *AccountChangeController {
	return &AccountChangeController{r: accountChangeRepository}
}

func (c *AccountChangeController) GetAccountChanges(ctx *gin.Context) {
	accountIdParsed := ctx.Param("id")
	accountId, err := strconv.ParseUint(accountIdParsed, 10, 32)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	accountChanges, err := c.r.GetAccountChanges(uint(accountId))
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, accountChanges)
}

func (c *AccountChangeController) CreateAccountChange(ctx *gin.Context) {
	accountIdParsed := ctx.Param("id")
	accountId, err := strconv.ParseUint(accountIdParsed, 10, 32)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var accountChangeCreateDto dto.AccountChangeCreateDto
	err = ctx.ShouldBindJSON(&accountChangeCreateDto)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	accountChange, err := c.r.CreateAccountChange(accountChangeCreateDto, uint(accountId))
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, accountChange)
}
