package repository

import "accountant/packages/account/entity"

type IAccountChangeCategoryRepository interface {
	CreateAccountChangeCategory() (entity.AccountChangeCategory, error)
}
