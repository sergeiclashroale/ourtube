package controller

import (
	"accountant/packages/account/dto"
	"accountant/packages/account/repository"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type AccountController struct {
	r repository.IAccountRepository
}

func NewAccountController(accountRepository repository.IAccountRepository) *AccountController {
	return &AccountController{r: accountRepository}
}

// swagger:route GET /accounts accounts
func (controller *AccountController) GetAllAccounts(ctx *gin.Context) {
	accounts, err := controller.r.GetAccounts()
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, accounts)
}

func (controller *AccountController) GetAccountById(ctx *gin.Context) {
	print(ctx.Param("id"))
	accountId, err := strconv.ParseUint(ctx.Param("id"), 10, 32)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	account, err := controller.r.GetAccountById(uint(accountId))
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, account)
}

func (controller *AccountController) CreateAccount(ctx *gin.Context) {
	var accountCreateDto dto.AccountCreateDto
	err := ctx.ShouldBindJSON(&accountCreateDto)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	account, err := controller.r.CreateAccount(accountCreateDto)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, account)
}
