package entity

import (
	"accountant/packages/general"
)

type AccountChangeCategory struct {
	general.Model
	Name    string          `json:"name"`
	Changes []AccountChange `json:"changes"`
}
