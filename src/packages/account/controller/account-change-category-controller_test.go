package controller

import (
	"accountant/packages/account/dto"
	"accountant/packages/account/entity"
	"bytes"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"net/http/httptest"
	"testing"
)

type accountChangeCategoryRepositoryMock struct{}

func (a accountChangeCategoryRepositoryMock) CreateAccountChangeCategory() (entity.AccountChangeCategory, error) {
	return entity.AccountChangeCategory{
		Name: "test",
	}, nil
}

func TestCreateAccountChangeCategory_should_return_created_account_change_category(t *testing.T) {
	// Given
	controller := NewAccountChangeCategoryController(accountChangeCategoryRepositoryMock{})

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)
	accountChangeCategoryCreateDto := dto.AccountChangeCategoryCreateDto{
		Name: "test",
	}
	body, _ := json.Marshal(accountChangeCategoryCreateDto)
	ctx.Request = httptest.NewRequest("POST", "/accounts/changes/categories", bytes.NewReader(body))

	// When
	controller.CreateAccountChangeCategory(ctx)

	// Then
	if w.Code != 201 {
		t.Errorf("expected status code 201, got %d", w.Code)
	}

	var result entity.AccountChangeCategory

	decoder := json.NewDecoder(w.Body)
	decoder.DisallowUnknownFields()

	err := decoder.Decode(&result)
	if err != nil {
		t.Errorf("expected response body to be account change category type, got %s", err.Error())
	}
}
