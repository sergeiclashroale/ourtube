package packages

import (
	"accountant/packages/account"
	"accountant/packages/docs"
	"accountant/packages/global"
	"go.uber.org/fx"
)

func GetApplicationModule() fx.Option {
	return fx.Options(
		global.GetConfigModule(),
		global.GetLoggerModule(),
		global.GetDatabaseModule(),
		docs.GetDocsModule(),
		account.GetAccountModule(),
		global.GetRouterModule(),
	)
}
