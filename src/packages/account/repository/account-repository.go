package repository

import (
	"accountant/packages/account/dto"
	"accountant/packages/account/entity"
	"errors"
	"go.uber.org/fx"
	"gorm.io/gorm"
)

type IAccountRepository interface {
	GetAccounts() ([]entity.Account, error)
	GetAccountById(accountId uint) (entity.Account, error)
	CreateAccount(dto dto.AccountCreateDto) (entity.Account, error)
}

type AccountRepository struct {
	db *gorm.DB
}

func AsIAccountRepository(repository any) any {
	return fx.Annotate(
		repository,
		fx.As(new(IAccountRepository)),
	)
}

func NewAccountRepository(db *gorm.DB) *AccountRepository {
	return &AccountRepository{db: db}
}

func (r *AccountRepository) GetAccounts() ([]entity.Account, error) {
	var accounts []entity.Account

	result := r.db.Preload("Changes").Find(&accounts)
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return accounts, nil
	}

	return accounts, result.Error
}

func (r *AccountRepository) GetAccountById(accountId uint) (entity.Account, error) {
	account := entity.Account{}

	result := r.db.Preload("Changes").First(&account, accountId)
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return account, errors.New("account not found")
	}

	return account, nil
}

func (r *AccountRepository) CreateAccount(dto dto.AccountCreateDto) (entity.Account, error) {
	changes := make([]entity.AccountChange, len(dto.Changes))
	for i, change := range dto.Changes {
		changes[i] = entity.AccountChange{
			Name:                    change.Name,
			Value:                   change.Value,
			AccountChangeCategoryID: change.AccountChangeCategoryID,
		}
	}

	account := entity.Account{Name: dto.Name, Changes: changes}

	result := r.db.Create(&account)
	if result.Error != nil {
		return account, result.Error
	}

	return account, nil
}
